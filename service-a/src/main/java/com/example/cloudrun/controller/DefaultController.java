package com.example.cloudrun.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.cloudrun.config.RestTemplateConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Map;
import org.apache.http.Header;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

@RestController
public class DefaultController {

    public static Log logger = LogFactory.getLog(DefaultController.class);

    @Autowired
    RestTemplateConfig restTemplateConfig;

    /**
     *
     * @return String
     */
    @RequestMapping(path = "/healthcheck", method = RequestMethod.GET)
    public ResponseEntity healthcheck() {

        String response = "";
        try {
            HttpHeaders headers = new HttpHeaders();
            final HttpEntity entity = new HttpEntity<>(headers);

            final ResponseEntity responseEntity = restTemplateConfig.restTemplate().exchange(
                    "http://service-b/healthcheck",
                    HttpMethod.GET,
                    entity,
                    String.class);
            logger.info("Status Code - " + responseEntity.getStatusCodeValue());
            response = responseEntity.getBody().toString();
        } catch (Exception e) {
            logger.error("healthcheck", e);
        }

        return ResponseEntity.ok(response);

    }

    /**
     *
     * @param request
     * @return decoded token value
     * @throws JWTDecodeException
     */

    @RequestMapping(path = "/identity", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity identity(HttpServletRequest request) {

        String authInfo = request.getHeader("x-apigateway-api-userinfo");
        byte data[] = Base64.getDecoder().decode(authInfo);
        return ResponseEntity.ok(new String(data));

    }

}
