package com.example.cloudrun.config;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Configuration

public class RestTemplateConfig {
    protected Log logger = LogFactory.getLog(RestTemplateConfig.class);

    @Bean
    public RestTemplate restTemplate() {
        logger.debug("RestTemplate-init-start");
        RestTemplate template = new RestTemplate();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(100);
        connectionManager.setDefaultMaxPerRoute(6);
        template.setRequestFactory(new HttpComponentsClientHttpRequestFactory(
                HttpClients.custom().setConnectionManager(connectionManager).build()));
        logger.info("Initialized RestTemplate - " + template);
        return template;
    }
}
